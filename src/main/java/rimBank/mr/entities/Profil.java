package rimBank.mr.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Profil implements Serializable{
@Id
private String typeProfile;
private String Description;

@ManyToMany(mappedBy = "profil", fetch = FetchType.LAZY)
private Collection<Utilisateur> utilisateurs;

public Profil() {
	super();
	// TODO Auto-generated constructor stub
}
public Profil(String typeProfile, String description) {
	super();
	this.typeProfile = typeProfile;
	Description = description;
}

public String getTypeProfile() {
	return typeProfile;
}

public void setTypeProfile(String typeProfile) {
	this.typeProfile = typeProfile;
}
public String getDescription() {
	return Description;
}
public void setDescription(String description) {
	Description = description;
}

public Collection<Utilisateur> getUtilisateurs() {
	return utilisateurs;
}
public void setUtilisateurs(Collection<Utilisateur> utilisateurs) {
	this.utilisateurs = utilisateurs;
}

}
