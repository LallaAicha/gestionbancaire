package rimBank.mr.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class CarteBancaire implements Serializable{
@Id @GeneratedValue
private Long numeroCarte ;
private String codeSecret ;

@ManyToOne
@JoinColumn(name="CODECPTE")
private CompteBancaire compteBancaire ;

public CarteBancaire() {
	super();
	// TODO Auto-generated constructor stub
}
public CarteBancaire(Long numeroCarte, String codeSecret, CompteBancaire compteBancaire) {
	super();
	this.numeroCarte = numeroCarte;
	this.codeSecret = codeSecret;
	this.compteBancaire = compteBancaire;
}
public Long getNumeroCarte() {
	return numeroCarte;
}
public void setNumeroCarte(Long numeroCarte) {
	this.numeroCarte = numeroCarte;
}
public String getCodeSecret() {
	return codeSecret;
}
public void setCodeSecret(String codeSecret) {
	this.codeSecret = codeSecret;
}
public CompteBancaire getCompteBancaire() {
	return compteBancaire;
}
public void setCompteBancaire(CompteBancaire compteBancaire) {
	this.compteBancaire = compteBancaire;
}


}
