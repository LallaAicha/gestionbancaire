package rimBank.mr.entities;

import java.util.Date;

import javax.persistence.*;
@Entity @DiscriminatorValue("CC")
//si je cree un compte courant la colonne typeCompte va prendre la valeur CC
public class CompteCourant extends CompteBancaire{
private double decouvert;

public CompteCourant() {
	super();
	// TODO Auto-generated constructor stub
}

public CompteCourant(String numCompte, Date dateCreation, double solde, Client client, double decouvert) {
	super(numCompte, dateCreation, solde, client);
	this.decouvert = decouvert;
}

public double getDecouvert() {
	return decouvert;
}

public void setDecouvert(double decouvert) {
	this.decouvert = decouvert;
}


}
