package rimBank.mr.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;

//la classe operation est abstraite parceque on va creer soit des versement soit des retrait et Operation nest pas instanciable
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="TYPEOP",discriminatorType=DiscriminatorType.STRING,length=15)
public abstract class Operation implements Serializable{
	
@Id @GeneratedValue
private Long numero;
private Date dateOperation;
private double montant;

@ManyToOne
@JoinColumn(name="CODECPTE")
private CompteBancaire compteBancaire ;

public Operation() {
	super();
	// TODO Auto-generated constructor stub
}
public Operation(Date dateOperation, double montant, CompteBancaire compteBancaire) {
	super();
	this.dateOperation = dateOperation;
	this.montant = montant;
	this.compteBancaire = compteBancaire;
}
public Long getNumero() {
	return numero;
}
public void setNumero(Long numero) {
	this.numero = numero;
}
public Date getDateOperation() {
	return dateOperation;
}
public void setDateOperation(Date dateOperation) {
	this.dateOperation = dateOperation;
}
public double getMontant() {
	return montant;
}
public void setMontant(double montant) {
	this.montant = montant;
}
public CompteBancaire getCompteBancaire() {
	return compteBancaire;
}
public void setCompteBancaire(CompteBancaire compteBancaire) {
	this.compteBancaire = compteBancaire;
}

}
