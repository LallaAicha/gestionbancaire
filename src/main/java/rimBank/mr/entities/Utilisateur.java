package rimBank.mr.entities;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Utilisateur implements Serializable{
	@Id @GeneratedValue
	private Long code ;
	
	private String nom ;
	private String prenom ;
	private String login ;
	private String motDePasse ;
	
	@ManyToOne
	@JoinColumn(name="TYPEPROFIL")
	private Profil profil;
	
	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Utilisateur(String login, String motDePasse) {
		super();
		this.login = login;
		this.motDePasse = motDePasse;
	}

	
	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public Profil getProfil() {
		return profil;
	}

	public void setProfil(Profil profil) {
		this.profil = profil;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getMotDePasse() {
		return motDePasse;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

}
