package rimBank.mr.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.*;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import java.util.Collection;
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="TYPECPTE",discriminatorType=DiscriminatorType.STRING,length=15)
public abstract class CompteBancaire implements Serializable{
    @Id 
	private String numCompte;
	private Date dateCreation;
	private double solde;
	
	@ManyToOne
	@JoinColumn(name="CODECLI")
	private Client client;
	
	@OneToMany(mappedBy="compteBancaire",fetch=FetchType.LAZY)
	private Collection<Operation> operations;
	
	@OneToMany(mappedBy="compteBancaire",fetch=FetchType.LAZY)
	private Collection<CarteBancaire> cartes;
	
	public CompteBancaire() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CompteBancaire(String numCompte, Date dateCreation, double solde, Client client) {
		super();
		this.numCompte = numCompte;
		this.dateCreation = dateCreation;
		this.solde = solde;
		this.client = client;
	}
	public String getNumCompte() {
		return numCompte;
	}
	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public double getSolde() {
		return solde;
	}
	public void setSolde(double solde) {
		this.solde = solde;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Collection<Operation> getOperations() {
		return operations;
	}
	public void setOperations(Collection<Operation> operations) {
		this.operations = operations;
	}
	public Collection<CarteBancaire> getCartes() {
		return cartes;
	}
	public void setCartes(Collection<CarteBancaire> cartes) {
		this.cartes = cartes;
	}

	
}
