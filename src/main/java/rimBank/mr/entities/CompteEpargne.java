package rimBank.mr.entities;

import java.util.Date;
import javax.persistence.*;
@Entity @DiscriminatorValue("CE")
public class CompteEpargne extends CompteBancaire{
private double taux;

public CompteEpargne() {
	super();
	// TODO Auto-generated constructor stub
}

public CompteEpargne(String numCompte, Date dateCreation, double solde, Client client, double taux) {
	super(numCompte, dateCreation, solde, client);
	this.taux = taux;
}

public double getTaux() {
	return taux;
}

public void setTaux(double taux) {
	this.taux = taux;
}


}
