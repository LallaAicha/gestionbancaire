package rimBank.mr.entities;

import java.util.Collection;

import javax.persistence.*;

import java.io.Serializable;

@Entity
public class Client extends Utilisateur{
	private String email ;
	private String adresse ;
	private Long telephone ;
	
	
	//Un client peut avoir plusiers comptes donc on utilise OneToMany
	@OneToMany(mappedBy="client",fetch=FetchType.LAZY)
	private Collection<CompteBancaire> comptes ;
	
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Client(String login, String motDePasse, String email) {
		super(login, motDePasse);
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public Long getTelephone() {
		return telephone;
	}
	public void setTelephone(Long telephone) {
		this.telephone = telephone;
	}
	
	public Collection<CompteBancaire> getComptes() {
		return comptes;
	}
	public void setComptes(Collection<CompteBancaire> comptes) {
		this.comptes = comptes;
	}


}
