package rimBank.mr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RimBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(RimBankApplication.class, args);
	}

}

