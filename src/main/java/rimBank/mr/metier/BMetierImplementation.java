package rimBank.mr.metier;

import java.text.SimpleDateFormat;
import java.util.Date;
import rimBank.mr.dao.ClientRepository;
import rimBank.mr.dao.CompteBancaireRepository;
import rimBank.mr.dao.OperationRepository;
import rimBank.mr.entities.Client;
import rimBank.mr.entities.CompteBancaire;
import rimBank.mr.entities.CompteCourant;
import rimBank.mr.entities.Operation;
import rimBank.mr.entities.Retrait;
import rimBank.mr.entities.Versement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//pour que spring puisse instancier cette classe au demmarage on utilise l'annotation service qui est utilisée pour les objets de la couche metier
@Service
@Transactional
public class BMetierImplementation implements BMetier{
//Pour consulter les comptes on besoin de la couche DAO (compteRepository)
	@Autowired
	private CompteBancaireRepository compteBancaireRepository;
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private OperationRepository operationRepository;
	
	SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'à' hh:mm:ss");
	
	@Override
	public CompteBancaire consulterCompte(String codeCpte) {
      CompteBancaire cp=compteBancaireRepository.findById(codeCpte).get();
      if(cp==null) throw new RuntimeException("Compte introuvable");
	  return cp;	
	}

	@Override
	public Client consulterClient(Long code) {
		Client client=clientRepository.findById(code).get();
	      if(client == null) throw new RuntimeException("Client introuvable");
		  return client;			
	}

	@Override
	public void verser(String codeCpte, double montant) {
		CompteBancaire cp=consulterCompte(codeCpte);
		Versement v=new Versement(new Date(), montant, cp);
		operationRepository.save(v);
		cp.setSolde(cp.getSolde() + montant);
		compteBancaireRepository.save(cp);
	}

	@Override
	public void retirer(String codeCpte, double montant) {
		CompteBancaire cp = consulterCompte(codeCpte);
		double facilitesCaisse = 0;
		if(cp instanceof CompteCourant)
			facilitesCaisse = ((CompteCourant) cp).getDecouvert();
		if(cp.getSolde() + facilitesCaisse < montant)
			throw new RuntimeException("Solde insuffisant");
		Retrait r = new Retrait(new Date(), montant, cp);
		operationRepository.save(r);
		cp.setSolde(cp.getSolde() - montant);
		compteBancaireRepository.save(cp);		
	}

	@Override
	public void virement(String codeCpte1, String codeCpte2, double montant) {
		if(codeCpte1.equals(codeCpte2)){
			throw new RuntimeException("Impossibile de faire un virement sur le meme compte");
		}
		retirer(codeCpte1,montant);
		verser(codeCpte2, montant);		
	}

	@Override
	public Page<Operation> listOperation(String codeCpte, int page, int size) {
		return operationRepository.listOperation(codeCpte, PageRequest.of(page, size));

	}

	
	
}
