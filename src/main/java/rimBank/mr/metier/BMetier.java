package rimBank.mr.metier;

import org.springframework.data.domain.Page;

import rimBank.mr.entities.Client;
import rimBank.mr.entities.Operation;
import rimBank.mr.entities.CompteBancaire;

public interface BMetier {

	public CompteBancaire consulterCompte(String codeCpte);
	public Client consulterClient(Long code);

	public void verser(String codeCpte, double montant);
	public void retirer(String codeCpte, double montant);
	public void virement(String codeCpte1, String codeCpte2, double montant);
	public Page<Operation> listOperation(String codeCpte, int page, int size);
	
}
