package rimBank.mr.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import rimBank.mr.entities.CarteBancaire;
public interface CarteBancaireRepository extends JpaRepository<CarteBancaire,Long> {

}
