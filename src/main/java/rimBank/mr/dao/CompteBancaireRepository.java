package rimBank.mr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import rimBank.mr.entities.CompteBancaire;

public interface CompteBancaireRepository extends JpaRepository<CompteBancaire,String>{

}
