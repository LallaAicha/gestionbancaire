package rimBank.mr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import rimBank.mr.entities.Profil;

public interface ProfilRepository extends JpaRepository<Profil,String>{

}
