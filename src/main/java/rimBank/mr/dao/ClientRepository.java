package rimBank.mr.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import rimBank.mr.entities.Client;
public interface ClientRepository extends JpaRepository<Client,Long>{

}
