package rimBank.mr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import rimBank.mr.entities.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur,Long>{

}
